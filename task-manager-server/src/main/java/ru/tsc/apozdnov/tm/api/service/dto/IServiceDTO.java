package ru.tsc.apozdnov.tm.api.service.dto;

import ru.tsc.apozdnov.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.apozdnov.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {

}